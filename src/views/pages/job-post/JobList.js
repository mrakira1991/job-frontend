import React, { Component } from 'react'
import Modal from 'react-bootstrap/Modal'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import {
  CTable,
  CTableHead,
  CTableRow,
  CTableBody,
  CTableDataCell,
  CForm,
  CFormInput,
  CButton,
  CTableHeaderCell,
  CCard,
  CRow,
  CCol,
  CFormLabel,
  CFormTextarea,
  CModalBody,
  CModalFooter,
} from '@coreui/react'
import JobService from '../../../services/JobService'
import EmployeeService from "../../../services/EmployeeService";

export default class JobList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      job: {},
      errorMessage: null,
      showAddJobDialog: false,
      tabKey: 'jobList',
      jobs: [],
      appliedJobs: []
    }

    this.jobService = new JobService()
    this.emplyeeService = new EmployeeService()
  }

  componentDidMount() {
    this.getData()
  }

  getData = () => {
    let user = JSON.parse(localStorage.getItem('user'))
    this.emplyeeService.findApplied(user.id).then((response) => {
      if (response) {
        this.setState({
          appliedJobs: response,
        })
      }
    })
        .catch((error) => {
          this.setState({
            loading: false,
            errorMessage: error,
          })
        })

    const qs = {}
    this.jobService
      .getAll(qs)
      .then((response) => {
        if (response.content) {
          this.setState({
            jobs: response.content,
          })
        }
      })
      .catch((error) => {
        this.setState({
          loading: false,
          errorMessage: error,
        })
      })
  }

  handle = (e, item) => {
    console.log(item)
    let user = JSON.parse(localStorage.getItem('user'))
    this.emplyeeService.applyJob(user.id, item.id).then((response) => {
      console.log(response)
      alert('Apply thành công')
    })
        .catch((error) => {
          console.error(error)
          alert('Apply thất bại')
        })
  }

  render() {
    return (
      <>
        <Tabs
          id="controlled-tab-example"
          activeKey={this.state.tabKey}
          onSelect={(k) => this.setState({ tabKey: k })}
          className="mb-3"
        >
          <Tab eventKey="jobList" title="Jobs">
            <CRow className="justify-content-center">
              <CCol md={8}>
                <h1>Jobs</h1>
              </CCol>
            </CRow>
            <CCard style={{ width: '100%' }}>
              <CTable>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">#</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Title</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Category</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Level</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {this.state.jobs.map((item, index) => {
                    return (
                      <CTableRow key={index}>
                        <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                        <CTableDataCell>{item.title}</CTableDataCell>
                        <CTableDataCell>
                          {item.jobCategory ? item.jobCategory.name : ''}
                        </CTableDataCell>
                        <CTableDataCell>
                          {item.experienceLevel ? item.experienceLevel.name : ''}
                        </CTableDataCell>
                        <CTableDataCell>
                          <CButton color="info" onClick={e => this.handle(e, item)}>
                            Apply
                          </CButton>
                        </CTableDataCell>
                      </CTableRow>
                    )
                  })}
                </CTableBody>
              </CTable>
            </CCard>
          </Tab>
          <Tab eventKey="appliedJob" title="Applied Jobs">
            <CRow className="justify-content-center">
              <CCol md={8}>
                <h1>Applied Jobs</h1>
              </CCol>
            </CRow>
            <CCard style={{ width: '100%' }}>
              <CTable>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">#</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Title</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Category</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Level</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {this.state.appliedJobs.map((item, index) => {
                    return (
                        <CTableRow key={index}>
                          <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                          <CTableDataCell>{item.title}</CTableDataCell>
                          <CTableDataCell>
                            {item.jobCategory ? item.jobCategory.name : ''}
                          </CTableDataCell>
                          <CTableDataCell>
                            {item.experienceLevel ? item.experienceLevel.name : ''}
                          </CTableDataCell>

                        </CTableRow>
                    )
                  })}
                </CTableBody>
              </CTable>
            </CCard>
          </Tab>
        </Tabs>
      </>
    )
  }
}
