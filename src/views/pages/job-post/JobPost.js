import React, { Component } from 'react'
import Modal from 'react-bootstrap/Modal'
import {
  CTable,
  CTableHead,
  CTableRow,
  CTableBody,
  CTableDataCell,
  CForm,
  CFormInput,
  CButton,
  CTableHeaderCell,
  CCard,
  CRow,
  CCol,
  CFormLabel,
  CFormTextarea,
} from '@coreui/react'
import JobService from '../../../services/JobService'
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select/Select";
import MasterDataService from "../../../services/MasterDataService";
import EmployerService from "../../../services/EmployerService";

export default class JobPost extends Component {
  constructor(props) {
    super(props)
    this.state = {
      job: {},
      exp_levels: [],
      cats: [],
      jobs: [],
      cat: '',
      errorMessage: null,
      showAddJobDialog: false,
    }

    this.jobService = new JobService()
    this.employerService = new EmployerService();
    this.masterDataService = new MasterDataService();
  }

  componentDidMount() {
    this.masterDataService.experienceLevels().then(res => {
      this.setState({exp_levels: res})
    })
    this.masterDataService.jobCategories().then(res => {
      this.setState({cats: res})
    })
      let user = JSON.parse(localStorage.getItem('user'))
      this.employerService.findJobs(user.id).then(res => {
        this.setState({
          jobs: res
        })
      })
  }

  doSubmit = () => {
    console.log(this.state.job)
    let user = JSON.parse(localStorage.getItem('user'))
    if(this.state.mode === 'create') {
      this.jobService
          .createJob(user.id, this.state.job)
          .then((response) => {
            console.log(response)
            this.onHideDialog()
          })
          .catch((error) => {
            console.error(error)
            this.setState({
              errorMessage: 'Lỗi hệ thống',
            })
          })
    } else {
      this.jobService
          .updateJob(this.state.job.id, this.state.job)
          .then((response) => {
            console.log(response)
            this.onHideDialog()
          })
          .catch((error) => {
            console.error(error)
            this.setState({
              errorMessage: 'Lỗi hệ thống',
            })
          })
    }
  }

  onShowDialog = (mode) => {
    this.setState({
      showAddJobDialog: true,
      mode
    })
  }

  onHideDialog = () => {
    this.setState({
      showAddJobDialog: false,
      job: {}
    })
  }

  onChangeFormValue = (field, value) => {
    let job = this.state.job
    job[field] = value
    console.log(job)
    this.setState({
      job: job,
    })
  }

  createJobDialog = () => {
    return (
      <Modal show={this.state.showAddJobDialog} onHide={this.onHideDialog}>
        <Modal.Header closeButton>
          <Modal.Title>Add job</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <CForm>
            <div className="mb-3">
              {this.state.errorMessage != null && (
                <CFormLabel htmlFor="title" color="danger">
                  {this.state.errorMessage}
                </CFormLabel>
              )}
            </div>
            <div className="mb-3">
              <CFormLabel htmlFor="title">Title</CFormLabel>
              <CFormInput
                type="text"
                id="title"
                value={this.state.job.title}
                onChange={(e) => this.onChangeFormValue('title', e.target.value)}
              />
            </div>
            <div className="mb-3">
              <CFormLabel htmlFor="description">Description</CFormLabel>
              <CFormTextarea
                id="description"
                rows="3"
                value={this.state.job.description}
                onChange={(e) => this.onChangeFormValue('description', e.target.value)}
              />
            </div>
            <div className="mb-3">
              <CFormLabel htmlFor="loc">Location</CFormLabel>
              <CFormInput
                  type="text"
                  id="loc"
                  value={this.state.job.location}
                  onChange={(e) => this.onChangeFormValue('location', e.target.value)}
              />
            </div>
            <div className="mb-3">
              <CFormLabel htmlFor="exp">Experience level</CFormLabel><br/>
              <Select
                  labelId="exp"
                  id="exp"
                  value={this.state.exp}
                  onChange={(e) => this.onChangeFormValue('experienceLevel', { id: e.target.value })}
              >
                {this.state.exp_levels.map(option => {
                  return (
                      <MenuItem key={option.id} value={option.id}>
                        {option.name}
                      </MenuItem>
                  )
                })}
              </Select>
            </div>
            <div className="mb-3">
              <CFormLabel htmlFor="abc">Category</CFormLabel><br/>
              <Select
                  labelId="abc"
                  id="abc"
                  value={this.state.cat}
                  onChange={(e) => this.onChangeFormValue('jobCategory', { id: e.target.value })}
              >
                {this.state.cats.map(option => {
                  return (
                      <MenuItem key={option.id} value={option.id}>
                        {option.name}
                      </MenuItem>
                  )
                })}
              </Select>
            </div>
          </CForm>
        </Modal.Body>
        <Modal.Footer>
          <CButton color="secondary" onClick={this.onHideDialog}>
            Close
          </CButton>
          <CButton color="primary" onClick={this.doSubmit}>
            Lưu
          </CButton>
        </Modal.Footer>
      </Modal>
    )
  }

  handle = (e, item) => {
    console.log(item)
    let tmp = Object.assign({}, item);
    this.setState({
      job: tmp,
      exp: tmp.experienceLevel.id,
      cat: tmp.jobCategory.id
    }, () => this.onShowDialog('update'))
  }

  render() {
    return (
      <>
        {this.createJobDialog()}
        <CRow className="justify-content-center">
          <CCol md={8}>
            <h1>Posted jobs</h1>
          </CCol>
          <CCol md={4}>
            <CButton color="info" onClick={() => this.onShowDialog('create')}>
              Add job
            </CButton>
          </CCol>
        </CRow>
        <CCard style={{ width: '100%' }}>
          <CTable>
            <CTableHead>
              <CTableRow>
                <CTableHeaderCell scope="col">#</CTableHeaderCell>
                <CTableHeaderCell scope="col">Title</CTableHeaderCell>
                <CTableHeaderCell scope="col">Category</CTableHeaderCell>
                <CTableHeaderCell scope="col">Level</CTableHeaderCell>
                <CTableHeaderCell scope="col">Location</CTableHeaderCell>
                <CTableHeaderCell scope="col">Action</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
              {this.state.jobs.map((item, index) => {
                return (
                    <CTableRow key={index}>
                      <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                      <CTableDataCell>{item.title}</CTableDataCell>
                      <CTableDataCell>
                        {item.jobCategory ? item.jobCategory.name : ''}
                      </CTableDataCell>
                      <CTableDataCell>
                        {item.experienceLevel ? item.experienceLevel.name : ''}
                      </CTableDataCell>
                      <CTableDataCell>
                        {item.location}
                      </CTableDataCell>
                      <CTableDataCell>
                        <CButton color="info" onClick={e => this.handle(e, item)}>
                          Edit
                        </CButton>
                      </CTableDataCell>
                    </CTableRow>
                )
              })}
            </CTableBody>
          </CTable>
        </CCard>
      </>
    )
  }
}
