import React, { Component } from 'react'
import Modal from 'react-bootstrap/Modal'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import {
    CTable,
    CTableHead,
    CTableRow,
    CTableBody,
    CTableDataCell,
    CForm,
    CFormInput,
    CButton,
    CTableHeaderCell,
    CCard,
    CRow,
    CCol,
    CFormLabel,
    CFormTextarea,
    CModalBody,
    CModalFooter,
} from '@coreui/react'
import JobService from '../../../services/JobService'
import EmployeeService from "../../../services/EmployeeService";
import EmployerService from "../../../services/EmployerService";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem";
import MasterDataService from "../../../services/MasterDataService";

export default class Admin extends Component {
    constructor(props) {
        super(props)
        this.state = {
            job: {},
            errorMessage: null,
            showAddJobDialog: false,
            tabKey: 'employees',
            employees: [],
            employer: [],
            jobs: [],
            cats: [],
            countByJobCategory: 0
        }

        this.jobService = new JobService()
        this.emplyeeService = new EmployeeService()
        this.emplyerService = new EmployerService()
        this.masterDataService = new MasterDataService();
    }

    componentDidMount() {
        this.getData()
    }

    getData = () => {
        this.masterDataService.jobCategories().then(res => {
            this.setState({cats: res})
        })

        const qs = {}
        this.emplyeeService
            .getAll(qs)
            .then((response) => {
                if (response.content) {
                    this.setState({
                        employees: response.content,
                    })
                }
            })
            .catch((error) => {
                this.setState({
                    loading: false,
                    errorMessage: error,
                })
            })

        this.emplyerService
            .getAll(qs)
            .then((response) => {
                if (response.content) {
                    this.setState({
                        employer: response.content,
                    })
                }
            })
            .catch((error) => {
                this.setState({
                    loading: false,
                    errorMessage: error,
                })
            })

        this.jobService
            .getAll(qs)
            .then((response) => {
                if (response.content) {
                    this.setState({
                        jobs: response.content,
                    })
                }
            })
            .catch((error) => {
                this.setState({
                    loading: false,
                    errorMessage: error,
                })
            })
    }

    handle = (e, item) => {
        console.log(item)
    }

    updateCountByJobCategory = (category) => {
        this.jobService.countByJobCategory(category).then(res => {
            this.setState({ countByJobCategory: res })
        })
    }

    render() {
        return (
            <>
                <Tabs
                    id="controlled-tab-example"
                    activeKey={this.state.tabKey}
                    onSelect={(k) => this.setState({ tabKey: k })}
                    className="mb-3"
                >
                    <Tab eventKey="employees" title="Employees">
                        <CRow className="justify-content-center">
                            <CCol md={8}>
                                <h1>Employees</h1>
                            </CCol>
                        </CRow>
                        <CCard style={{ width: '100%' }}>
                            <CTable>
                                <CTableHead>
                                    <CTableRow>
                                        <CTableHeaderCell scope="col">#</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Address</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Phone</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Location</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                                    </CTableRow>
                                </CTableHead>
                                <CTableBody>
                                    {this.state.employees.map((item, index) => {
                                        return (
                                            <CTableRow key={index}>
                                                <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                                                <CTableDataCell>{item.name}</CTableDataCell>
                                                <CTableDataCell>
                                                    {item.address}
                                                </CTableDataCell>
                                                <CTableDataCell>
                                                    {item.phone}
                                                </CTableDataCell>
                                                <CTableDataCell>
                                                    {item.location}
                                                </CTableDataCell>
                                                <CTableDataCell>
                                                    <CButton color="info" onClick={e => this.handle(e, item)}>
                                                        Edit
                                                    </CButton>
                                                </CTableDataCell>
                                            </CTableRow>
                                        )
                                    })}
                                </CTableBody>
                            </CTable>
                        </CCard>
                    </Tab>
                    <Tab eventKey="employer" title="Employer">
                        <CRow className="justify-content-center">
                            <CCol md={8}>
                                <h1>Employer</h1>
                            </CCol>
                        </CRow>
                        <CCard style={{ width: '100%' }}>
                            <CTable>
                                <CTableHead>
                                    <CTableRow>
                                        <CTableHeaderCell scope="col">#</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Address</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Phone</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Location</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                                    </CTableRow>
                                </CTableHead>
                                <CTableBody>
                                    {this.state.employer.map((item, index) => {
                                        return (
                                            <CTableRow key={index}>
                                                <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                                                <CTableDataCell>{item.name}</CTableDataCell>
                                                <CTableDataCell>
                                                    {item.address}
                                                </CTableDataCell>
                                                <CTableDataCell>
                                                    {item.phone}
                                                </CTableDataCell>
                                                <CTableDataCell>
                                                    {item.location}
                                                </CTableDataCell>
                                                <CTableDataCell>
                                                    <CButton color="info" onClick={e => this.handle(e, item)}>
                                                        Edit
                                                    </CButton>
                                                </CTableDataCell>
                                            </CTableRow>
                                        )
                                    })}
                                </CTableBody>
                            </CTable>
                        </CCard>
                    </Tab>
                    <Tab eventKey="jobs" title="Jobs">
                        <CRow className="justify-content-center">
                            <CCol md={8}>
                                <h1>Jobs</h1>
                            </CCol>
                        </CRow>
                        <CCard style={{ width: '100%' }}>
                            <CTable>
                                <CTableHead>
                                    <CTableRow>
                                        <CTableHeaderCell scope="col">#</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Title</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Category</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Level</CTableHeaderCell>
                                        <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                                    </CTableRow>
                                </CTableHead>
                                <CTableBody>
                                    {this.state.jobs.map((item, index) => {
                                        return (
                                            <CTableRow key={index}>
                                                <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                                                <CTableDataCell>{item.title}</CTableDataCell>
                                                <CTableDataCell>
                                                    {item.jobCategory ? item.jobCategory.name : ''}
                                                </CTableDataCell>
                                                <CTableDataCell>
                                                    {item.experienceLevel ? item.experienceLevel.name : ''}
                                                </CTableDataCell>
                                                <CTableDataCell>
                                                    <CButton color="info" onClick={e => this.handle(e, item)}>
                                                        Edit
                                                    </CButton>
                                                </CTableDataCell>
                                            </CTableRow>
                                        )
                                    })}
                                </CTableBody>
                            </CTable>
                        </CCard>
                    </Tab>
                    <Tab eventKey="statistic" title="Statistic">
                        <CRow className="justify-content-center">
                            <CCol md={8}>
                                <h1>Statistic</h1>
                            </CCol>
                        </CRow>
                        <CCard style={{ width: '50%' }}>
                            Job statistic based on category
                            <div className="mb-3">
                                <CFormLabel htmlFor="abc">Select category</CFormLabel><br/>
                                <Select
                                    labelId="abc"
                                    id="abc"
                                    value={this.state.cat}
                                    onChange={(e) => this.updateCountByJobCategory({ id: e.target.value })}
                                >
                                    {this.state.cats.map(option => {
                                        return (
                                            <MenuItem key={option.id} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        )
                                    })}
                                </Select>
                                <div>
                                    {`There are ${this.state.countByJobCategory} jobs based on this category`}
                                </div>
                            </div>
                        </CCard>
                        {/*<CCard style={{ width: '50%' }}>*/}
                        {/*    vvvvvvvvvvvvv*/}
                        {/*</CCard>*/}
                        {/*<CCard style={{ width: '50%' }}>*/}
                        {/*    vvvvvvvvvvvvvv*/}
                        {/*</CCard>*/}
                    </Tab>
                </Tabs>
            </>
        )
    }
}
