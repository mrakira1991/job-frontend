import * as ApiUrls from '../constants/apiUrls'
import BaseRequestService from './BaseRequestService'
import queryString from 'query-string'

export default class EmployerService {
  constructor() {
    this.baseRequestService = new BaseRequestService()
  }

  findJobs = (id) => {
    return this.baseRequestService
      .get(`${ApiUrls.BASE_API_URL}/employers/${id}/jobs`)
      .then((response) => response.data)
  }

  getAll = (qs) => {
    const query = queryString.stringify(qs)
    return this.baseRequestService
      .get(`${ApiUrls.BASE_API_URL}/employers?${query}`)
      .then((response) => response.data)
  }
}
