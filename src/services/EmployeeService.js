import * as ApiUrls from '../constants/apiUrls'
import BaseRequestService from './BaseRequestService'
import queryString from 'query-string'

export default class EmployeeService {
  constructor() {
    this.baseRequestService = new BaseRequestService()
  }

  applyJob = (eid, jid) => {
    return this.baseRequestService
      .post(`${ApiUrls.BASE_API_URL}/employees/${eid}/jobs/${jid}/apply`)
      .then((response) => response.data);
  }

  getAll = (qs) => {
    const query = queryString.stringify(qs)
    return this.baseRequestService
        .get(`${ApiUrls.BASE_API_URL}/employees?${query}`)
        .then((response) => response.data)
  }

  findApplied = (eid) => {
    return this.baseRequestService
        .get(`${ApiUrls.BASE_API_URL}/employees/${eid}/jobs`)
        .then((response) => response.data)
  }
}
